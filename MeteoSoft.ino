#include <SPI.h>
#include <SD.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <DHT.h>
#include <Adafruit_Sensor.h>

#define CMD_START '>'
#define DHT_PIN 3
#define DHT_TYPE DHT22

OneWire oneWire(2);
DallasTemperature temp(&oneWire);
File logFile;
DHT dht(DHT_PIN, DHT_TYPE);

char flg = ' ';
int sensType = 0;
int H = 0;
int M = 0;
int S = 0;
float lat;
float longit;
float alt;
float temperature;
float hum;
bool a;
bool b;


void setup() {
  Serial.begin(9600);
  temp.begin();
  dht.begin();
  pinMode(13, OUTPUT);
  
  if (!SD.begin(10)) {
    //Initializes the SD library and card
    return;
  }
}

void loop() {
  if (Serial.available() < 1 ) {
    return;
  }

  flg = Serial.read();
  //Wait for serial connection
  if (flg != CMD_START) {
    return;
  }
  
  sensType = Serial.parseInt();
  //parseInt() returns the first valid (long) integer number from the serial buffer
  
  if (sensType == 95) {
    //95 - Time (Hour, Minute, Second)
    int count = Serial.parseInt();
    H = Serial.parseInt();
    M = Serial.parseInt();
    S = Serial.parseInt();
    a = true;
  }

  if (sensType == 98) {
    //98 - GPS1 (Lat., Long., Alt.)
    int count2 = Serial.parseInt();
    lat = Serial.parseFloat();
    longit = Serial.parseFloat();
    alt =  Serial.parseFloat();
    b = true;
  }

  if (sensType == 99) {
    //99 - GPS2 (Bearing, Speed, Date/Time)
    int count = Serial.parseInt();
    float brg = Serial.parseFloat();
    float spd = Serial.parseFloat();
    float date = Serial.parseFloat();
  }

  if (a && b) {
    getSensorData();
    writeLog();
    a = false;
    b = false;
    Serial.flush();
  } else return;
  delay(500);
}

void getSensorData() {
  temp.requestTemperatures();
  temperature = temp.getTempCByIndex(0);
  hum = dht.readHumidity();
}

void writeLog() {
  digitalWrite(13, HIGH);
  logFile = SD.open("DATA.txt", FILE_WRITE);
  if ((logFile) && (lat != 0.0f)) {
    //If logfile and GPS available write data to SD card
    String inData = "";
    inData += String(H);
    inData += ":";
    inData += String(M);
    inData += ":";
    inData += String(S);

    inData += '_';
    inData += String(lat, 5);
    inData += '_';
    inData += String(longit, 5);
    inData += '_';
    inData += String(alt);

    inData += '_';
    inData += String(temperature);
    inData += '_';
    inData += String(hum);

    logFile.println(inData);
    logFile.close();
  }
}
