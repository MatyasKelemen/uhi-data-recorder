# UHI data recorder

Arduino based temperature and humidity measuring device for urban heat island research.

The device writes into the DATA.TXT file in every 10 second, in the following form:

*HH:MM:SS_Latitude_Longitude_Altitude_Temperature_Humidity*

**Necessary parts:**
- Android cell phone whit GPS and bluetooth.
- Arduino Uno, SD shield, SD card (FAT16 or FAT32)
- DS18B20 temperature sensor.
- DHT22 or AM2302 humidity sensor.
- HC-05 BT module.


**Android app:**
[SensoDuino](https://play.google.com/store/apps/details?id=com.techbitar.android.sensoduino)
(Android 2.3.3 – 4.4)

**Required Libraries:**
[Adafruit Unified Sensor Driver,](https://github.com/adafruit/Adafruit_Sensor)
[DallasTemperature,](https://github.com/milesburton/Arduino-Temperature-Control-Library)
[DHT,](https://github.com/adafruit/DHT-sensor-library)
[OneWire](https://github.com/PaulStoffregen/OneWire)

**License:**
The source code is under GPLv3.